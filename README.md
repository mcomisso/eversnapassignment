#iOS Assignment for Eversnap

The goal is to fetch "birthday" pictures from rest api of Flickr service, while maintaining an abstract implementation of the api service used in the controllers.



#Assignment Description:

##iOS Assignment
Welcome to the iOS developer candidate assignment.

Thanks for your interest in the Eversnap Team. You are 2 steps away from joining a world-class team of passionate & ambitious individuals.

Your task is to implement a photo gallery using Flickr API, with photos tagged _“Birthday”_ and a detail page where the user can take a closer look to the photo when tapping on the thumbnail.

You can use relevant (and supported) frameworks to reduce the amount of code required, best software practices, building scripts, etc...

This assignment is meant to be a proof of your skills, so we recommend to use as few libraries as possible. Libraries make the developer’s life easier and faster. But you need to be careful that the libraries chosen do not limit your control of the app and allow you to be able to customize it and most importantly, think long-term. In this direction, we also ask you not use storyboards or nib files, as we don’t use them in our app.
 
###IMPORTANT:
Do NOT use Flickr official Objective-C library, we would like you to use libraries that are reusable on other services, imagine that instead of Flickr you’re working with a different company API.
Bonus points 
You have to handle this assignment as if you were doing it in real life. So try to make the app available for most users (for example >=iOS7), consider slow connection situations, low memory situations, create an attractive UI, and so on...

Also, if you have time, feel free to add more functionalities like “photos near me”, navigating through photos while on detail view, etc...  

We love code conventions and comments :)

###Estimated time 
This assignment is planned to take roughly 2 days.  

###Grading Criteria
In the order of importance:
- Functionality: The app functions as it is described in the assignment (4 points)
- Quality of Code: The code doesn’t have conceptual errors, follows general conventions and guidelines of … (7 points)
- Memory management: The application won’t crash if added more functionalities (4 points)
- User Experience: The application is usable (4 points)
- Visual Design: The app is simple and well designed (3 points)
- Chosen Libraries: The chosen libraries are well supported, efficient, and well implemented (3 points)


##Frequently Asked Questions

##Does the application need to be done for both iPhone & iPad? 
No, it doesn’t. Focus on iPhone.
##Which libraries can I use? 
This is open, but try to choose widely supported, open-source libraries, with an open mind when choosing them. Which library would you use in your own application and why? 
##What happens if I don’t finish in time? 
Leave the assignment as it is, just make sure it builds and works. 2 days of work is enough to see your skills.